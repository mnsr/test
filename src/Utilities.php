<?php

namespace Mnsr\Test;

use Exception;

class Utilities
{

    /**
     * @param $file_full_path
     * @return int
     * @throws Exception
     */
    function getFileRowsCount($file_full_path)
    {
        if (!is_file($file_full_path)) throw new Exception("File ($file_full_path) doesn't exist !");
        return intval(exec("wc -l '$file_full_path'"));
    }

}
